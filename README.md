# Website Text Extractor

## Description

The Website Text Extractor is a program that allows users to extract all the text from a specified URL.

## Installation

To use the Website Text Extractor, users must have the following software:

- Python 3.x
- Pysimplegui
- Beautifulsoup

## How to Use

- Open the Website Text Extractor program.
- Input the URL of the website you wish to scrape.
- Click "Get text from website".
- Wait for the program to scrape the website.
- Once complete, the extracted text will be displayed in the GUI.
